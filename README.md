# Sujet2_Loizillon_Lagarrigue

Projet GitLab contenant le code du projet en C pour le sujet 2.

## Installation
Installez le projet avec cette commande dans le terminal :
```bash
git clone https://gitlab.com/sujet2_loizillon_lagarrigue/ProjetC.git
```

Puis allez dans le répertoire :
```bash
cd ProjetC
```

## Utilisation
Compilez le projet avec la commande :
```bash
make
```
Puis exécutez avec :
```bash
./projet
```

## Crédit
Loizillon Guillaume / Lagarrigue Adrien