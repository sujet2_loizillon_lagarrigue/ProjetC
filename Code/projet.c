#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>


// Fonction pour calculer la valeur binaire d'un entier
char* int_to_binary(int num) {
    char* binary_string = malloc(9 * sizeof(char));

    for (int i = 7; i >= 0; i--) {
        binary_string[7 - i] = ((num >> i) & 1) + '0';
    }
    binary_string[8] = '\0';

    return binary_string;
}

// Fonction pour passer d'une ip -> ip en binaire
char* ip_2_bin(char* ip) {
    char* binary_ip = malloc(33 * sizeof(char));
    binary_ip[0] = '\0';
    char ip_tmp[16];
    strcpy(ip_tmp, ip);

    char* token = strtok(ip_tmp, ".");
    while (token != NULL) {
        int octet = atoi(token);
        char* binary_octet = int_to_binary(octet);

        int len = strlen(binary_octet);
        for (int i = 0; i < 8 - len; i++) {
            strcat(binary_ip, "0");
        }

        strcat(binary_ip, binary_octet);
        token = strtok(NULL, ".");
    }

    return binary_ip;
}

// Fonction pour passer d'une ip en binaire -> ip
char* bin_2_ip(char* bin_ip) {
    char* result = malloc(16 * sizeof(char));
    int num_temp = 0;
    int index = 0;

    for (int i = 0; i < 32; i++) {
        if (bin_ip[i] == '1') {
            num_temp += pow(2, 7 - (i % 8));
        }
        if ((i + 1) % 8 == 0) {
            sprintf(result + index, "%d", num_temp);
            index += strlen(result + index);
            if (i != 31) {
                result[index++] = '.';
            }
            num_temp = 0;
        }
    }
    result[index] = '\0';

    return result;
}

// Fonction pour vérifier le format de l'adresse IP
bool checkIPFormat(const char* ip) {
    int count = 0;
    char ipCopy[20];
    strcpy(ipCopy, ip);
    char* token = strtok(ipCopy, ".");

    while (token != NULL) {
        int octet = atoi(token);
        if (octet < 0 || octet > 255) {
            return false;
        }
        count++;
        token = strtok(NULL, ".");
    }

    return count == 4;
}


// Fonction pour vérifier le format CIDR
bool checkCIDRFormat(int cidr) {
    return cidr >= 0 && cidr <= 32;
}

// Fonction pour récupérer l'adresse IP et CIDR
void getIP(char* ip, int* cidr) {
    while (1) {
        printf("Entrez une adresse IP au format xxx.xxx.xxx.xxx/cidr : ");
        char input[50];
        fgets(input, sizeof(input), stdin);

        if (sscanf(input, "%15[^/]/%d", ip, cidr) != 2) {
            printf("Format invalide. Assurez-vous d'utiliser le format xxx.xxx.xxx.xxx/cidr.\n");
        } else if (!checkIPFormat(ip)) {
            printf("L'adresse IP n'est pas dans le bon format.\n");
        } else if (!checkCIDRFormat(*cidr)) {
            printf("Le CIDR n'est pas dans le bon format.\n");
        } else {
            break;
        }
    }
}


// Fonction pour déterminer le type d'adresse IP
char* getTypeAdresse(const char* ip) {
    if (strcmp(ip, "127.0.0.1") == 0) {
        return "Localhost";
    } else if (strstr(ip, "255.255.255.255") != NULL) {
        return "Broadcast";
    } else if (strstr(ip, "224.") != NULL || strstr(ip, "239.") != NULL) {
        return "Multicast";
    } else if (strstr(ip, "10.") == ip || strstr(ip, "172.") == ip || strstr(ip, "192.168.") == ip) {
        return "Privée";
    } else {
        return "Publique";
    }
}

// Fonction pour déterminer la classe de l'adresse IP
char getClassAdresse(const char* ip) {
    char ip_tmp[16];
    strcpy(ip_tmp, ip);
    int premierOctet = atoi(strtok((char*)ip_tmp, "."));

    if (premierOctet >= 1 && premierOctet <= 126) {          //si l'adresse IP commence par 0 donc cidr = 16
        return 'A';
    } else if (premierOctet >= 128 && premierOctet <= 191) { //si l'adresse IP commence par 10 cidr = 8
        return 'B';
    } else if (premierOctet >= 192 && premierOctet <= 223) { //si l'adresse IP commence par 110 cidr = 24
        return 'C';
    } else if (premierOctet >= 224 && premierOctet <= 239) {
        return 'D';
    } else {
        return 'E';
    }
}

// Fonction pour déterminer le masque du réseau
char* getMasqueReseau(int cidr) {
    char* masqueReseau = malloc(sizeof(char) * 33);
    int n = 0;
    for (int i = 0; i < 32; i++) {
        if (i < cidr) {
            masqueReseau[n++] = '1';
        } else {
            masqueReseau[n++] = '0';
        }
    }
    masqueReseau[n] = '\0';

    return masqueReseau;
}

// Fonction pour déterminer l'adresse réseau
char* getAdresseReseau(char ip[], char masque[]) {
    char* AdresseReseau = malloc(sizeof(char) * 33);
    for (int i = 0; i < 32; i++) {
        if (masque[i] == '1') {
            AdresseReseau[i] = ip[i];
        } else {
            AdresseReseau[i] = '0';
        }
    }
    AdresseReseau[32] = '\0';

    return AdresseReseau;
}

// Fonction pour déterminer l'adresse de l'hôte
char* getAdresseHote(char ip[], char masque[]) {
    char* AdresseReseau = malloc(sizeof(char) * 33);
    for (int i = 0; i < 32; i++) {
        if (masque[i] == '0') {
            AdresseReseau[i] = ip[i];
        } else {
            AdresseReseau[i] = '0';
        }
    }
    AdresseReseau[32] = '\0';

    return AdresseReseau;
}


void main() {
    char ip[20];
    int cidr;
    getIP(ip, &cidr);
    char* ip_bin = ip_2_bin(ip);
    char* masque = getMasqueReseau(cidr);

    printf("Adresse ip en binaire : %s\n", ip_bin);
    printf("Classe d'adresse : %c\n", getClassAdresse(ip));
    printf("Type d'adresse   : %s\n", getTypeAdresse(ip));
    printf("Masque réseau    : %s\n", bin_2_ip(masque));
    printf("Adresse réseau   : %s\n", bin_2_ip(getAdresseReseau(ip_bin, masque)));
    printf("Adresse hôte     : %s\n", bin_2_ip(getAdresseHote(ip_bin, masque)));
}
